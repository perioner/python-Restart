# FiveM Server Manager

A simple yet powerful and useful tool to help you administrate your FiveM server.

[![Version](https://img.shields.io/badge/version-1.0-blue.svg)](http://localhost:8080)
[![Python Requirement](https://img.shields.io/badge/python-3.7-green.svg)](https://www.python.org/ftp/python/3.7.3/python-3.7.3.exe)

## Introduction

*This work is registered under the MIT License (C) Fernando Picoral 2019*

This manager **do not** display anything in game, it only controls the restarts of the server. To inform players of an upcoming server reboot, you will need to use a server plugin for that. Any ideas are welcome! Please share them in the post on the FiveM Forum. If you run into any bugs, also let me know on the FiveM Forum.

## Table of Contents

* [Installation](#installation)
* [Config File](#config)
* [Multiplatform](#multiplatform)
* [Help](#help)
* [Developers](#developers)


## Installation

For any other OS other than Windows (7 or later), see [Multipltaform](#multiplatform).
You can either use the manager by running the executable or the python script (recommended).

### Python Script

First, you need to have python installed on your computer. The recommended version is 3.7 or newer, however, any 3.x version should work just fine. If you don't have python installed, [click here](https://www.python.org/ftp/python/3.7.3/python-3.7.3.exe) to download the version 3.7.3

You also need to set the path for the `config.json` file on the `manager.py`. Open `manager.py` and around line 9 you will see:
```python
config_path = r"C:/Path/To/config.json"
```
Put the path to your `config.json` file there and pay attention not to delete the 'r' before the quotes.
You are ready to go! All you need to do is configure the `config.json` file. See more instructions on that [here.](#config)

### Executable

#### NOT AVAILABLE YET

If you want to run using the executable, you probably will have to disable your antivirus and Windows Defender. If you don't feel comfortable doing that, you may do the conversion to `.exe` on your own using [Py2Exe](http://www.py2exe.org/).

With all the antivirus disabled and the `config.json` properly configured, all you need to do is run the executable from the terminal using the path to the config file as a parameter:

```
Manager.exe "C:/Path/To/config.json"
```

**Important:** remember to use double quotes when the path or the name of the file has spaces. You may only use slashes. Backslashes (\\) won't work.

Since the executable is not the recommended option, no support whatsoever will be provided for bugs involving this option.

## Config File

The configuration file `config.json` is a vital part for the manager. In order to the manager work as intended, the config file must be perfect. Any mistakes will lead to the manager breaking completely. All settings are verified when you first start the manager and, hopefully, if something is not as it's supposed to be, you will be displayed an error. If you run into a bug which does not show any errors, please inform that on the FiveM forum post.

### Default Values  
 If your file is broken, here are the default values. The explanation for every setting is available below.
```json
{
  "lang": {
    "ui":"en",
    "system":"pt",
    "supported":["en", "pt"],
    "files_paths": {
      "en":"languages/english.json",
      "pt":"languages/portuguese.json"
    }
  },

  "manager": {
    "priority":"normal",
    "title":"Server Manager",
    "engine-path":"engine.bat",
    "readme-path":"README.md"
  },

  "server": {
    "name":"My Server",
    "ip":"localhost:30120",
    "path":"C:/FXServer",
    "cmd-title":"My Server",
    "start-server-cmd-path":"Start_Server.cmd",
    "priority":"realtime",
    "reboots":["06:00", "12:00", "18:00", "00:00"]
  },

  "backup": {
    "backup":true,
    "files":["resources", "server.cfg"],
    "location":"C:/FXServer/Backups",
    "date":["MM","DD","YYYY"],
    "prefix":"Backup - "

  },

  "log": {
    "log":true,
    "prefix":"Server Manager Log - ",
    "date":["MM","DD","YYYY"],
    "type":".log",
    "location":"C:/FXServer/Logs"
  },

  "browser": {
    "image":"chrome.exe",
    "close-on-start":false,
    "close-on-reboot":false
  },

  "test-server": {
    "active":false,
    "name":"My Test Server",
    "ip":"localhost:30121",
    "path":"C:/FXTestServer",
    "close-on-start":false,
    "close-on-reboot":false,
    "cmd-title":"My Test Server"
  }
}
```

### Explanations

*Definitions*
* **key** - a setting
* **block** - the sections which the keys are divided by

 #### Lang
 Has all the keys related to language.
 * ui - language for the manager and logs. Has to be on the supported array.
 * system - language of your OS
 * supported - array with all the languages. Each one must have a corresponding language file
 * file_paths - dictionary of paths for the languages files

#### Manager
Controls the manager settings
* priority - process priority of the manager
* title - title for the manager's CMD window
* engine-path - path for the engine.bat
* readme-path - path for the readme.md file

#### Server
One of the most important blocks; controls the information about your server
* name - your server's name
* ip - your server's ip
* path - path to the server folder
* cmd-title - title for the server's CMD window
* start-server-cmd-path - path for the Start_Server.cmd file
* priority - server's process priority
* reboots - an array of reboots using the 24h time format. You may have as many reboots as you want

#### Backup
Information about the files to be saved and where
* backup - backups happen on every restart. Set it as 'false' to turn it off
* files - an array of files and folders to be saved. Folders are everything that doesn't have a dot (.) on the name, therefore, don't use folders with dots on its name. All paths are from the server path on [server][path]
* location - where you want the backups to be saved
* date - how do you want the date on the backup's name. You may change the order but do not change the content of this array
* prefix - what you want on the backup's name before the date. Remember to add a space in the end and not to use unsupported characters for files names

#### Log
Settings about the manager's logs
* log - set it to 'false' to disable logging
* prefix - what you want on the log's name before the date. Remember to add a space in the end and not to use unsupported characters for files names
* date - how do you want the date on the log's name. You may change the order but do not change the contenent of this array
* type - what type of file you want (.log, .txt, .docx)
* location - where you want the logs to be stored

#### Browser
Settings to close the browser on start and/or restart
* image - browser's executable name. If you are not going to use this, let it as '.exe'
* close-on-start - set it to 'false' to disable this function
* close-on-reboot - set it to 'false' to disable this function

#### Test Server
Settings to handle the test server. If you don't have one, set [active] as 'false' to disable this block
* active - to disable this whole block, set it to 'false'
* name - name of your test server
* ip - ip of your test server
* path - path to your test server folder
* close-on-start - make sure the test server is closed when the main server starts
* close-on-reboot - make sure the test server is closed on every reboot of the main server
* cmd-title - title for the test server's CMD window (future applications)

## Multiplatform

The FiveM Server Manager was originally built for Windows but, since it's a python application, with a few modifications it can work with Linux and Mac. In order to make the manager compatible with MacOS or Linux, you will have to do some modification on the source code.

The only Windows-exclusive feature is the way priority is set, therefore you will need to delete the windows libraries imports, the `set_priority(pid=None, priority="normal")` method and its calls. You will also need to do some modification on the system calls (`os.system`) to adapt your OS.

If you create a working version for Linux and/or MacOS, consider sharing it with the community!
Support is only available for the official Windows version.

## Help

If you need help with the manager, use the post on the FiveM Forum. I wasn't able to do a lot of testing so you might encounter some bugs. Please report them so they can be fixed as fast as possible.

## Developers

Feel free to contribute to the development of this manager! I didn't upload it to GitHub (yet). When I do so, the link will be available on the FiveM Forum and the source code.

You may share this application with or without modifications, since you give me the credits and provide a link for the original version.

Search for 'TODO' on the source code to take a look at what needs to be fixed/improved.
Avoid using the `engine.bat`. I did not remove it since it might be useful in the future but, for now, `os.system` can handle everything with no problems.