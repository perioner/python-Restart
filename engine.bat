@echo off
setlocal enabledelayedexpansion

SET MODE=%1

REM ### NOT USED ###
REM Backup Function
REM engine.bat [backup] [src] [dst] [file/folder]
IF "%MODE%"=="backup" (
  SET SRC=%2
  SET DST=%3
  SET TYPE=%4

  IF "!TYPE!"=="folder" (
    xcopy !SRC! !DST! /S /C /Q /I
  ) ELSE (
    copy !SRC! !DST!
  )
)


REM ### NOT USED ###
REM Delete Function
REM engine.bat [delete] [path] [file/folder]
IF "%MODE%"=="delete" (
  SET LOC=%2
  SET TYPE=%3

  IF "!TYPE!"=="folder" (
    rmdir !LOC! /S /Q
  ) ELSE (
    del !LOC! /F /Q
  )
)

REM ### NOT USED ###
REM Close Browser
REM engine.bar [cbrowser] [image]
IF "%MODE%"=="cbrowser" (
  SET IMG=%2

  taskkill /F /IM !IMG! /T
	tasklist /FI "IMAGENAME eq !IMG!" 2>NUL | find /I /N "!IMG!">NUL
)

REM ### NOT USED ###
REM Start the server
REM engine.bat [start] [cmd_title] [server_path] [start_cmd_path]
IF "%MODE%"=="start" (
  SET CTITLE=%2
  SET SLOC=%3
  SET CLOC=%4

  start !CLOC! "!CTITLE!" "!SLOC!"
)
