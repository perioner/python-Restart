@echo off

SET FST_Param=%1
SET SND_Param=%2

REM Remove Quotes:
SET FST_Param=%FST_Param:"=%
SET SND_Param=%SND_Param:"=%

REM Set as Title:
title %FST_Param%

REM Call Server
"%SND_Param%\ServerStarter.bat" +exec server.cfg