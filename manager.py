# Fernando Picoral (C) 2019 - MIT License
# GTA V FiveM Framework Server Side Manager

# Read the README.md for instructions

#Path for the config.json file
config_path = r'C:/Users/Administrator/Desktop/manager/config.json'

### DON'T EDIT BELOW THIS LINE UNLESS YOU KNOW EXACTLY WHAT YOU ARE DOING ###

import time
import datetime
import os
import subprocess
import win32gui,win32process,signal,psutil
from locale import windows_locale
from ctypes import windll
from json import load as jload

# Search for 'TODO' to work on updates
# Future Ideais:
# - Add more precision to reboots (H:M:S)
# - Create GUI with Tkinter

#TODO - Make executable compatible

version = 1.0

def load_config():
    #Loads the config.json to 'config' and returns True if all values are valid
    #TODO: Add more secure verification to date arrays

    global config
    global lang #Set on the end of the function
    global engine #Not being used anywhere (06 May 2019)

    with open(config_path) as file:
        config = jload(file)
        engine = config['manager']['engine-path']

    ## Check Value
    ## Error messages only in english because config was not loaded successfully

    try:
        #Missing Key
        keys = list(config.keys())
        if len(keys) != 7:
            print("[CONFIG.JSON CRITICAL ERROR] - Your 'config.json' is missing a block. Re-download it.")
            return False

        #Lang
        lang_keys = config['lang'].keys()
        if len(lang_keys) != 4:
            print("[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'lang'")
            return False
        for key in lang_keys:
            if not key in ["ui", "system", "supported", "files_paths"]:
                print(f"[CONFIG.JSON CRITICAL ERROR] - '{key}' missing on language settings.")
                return False
        if len(config['lang']['supported']) > len(config["lang"]["files_paths"].keys()):
            print("[CONFIG.JSON CRITICAL ERROR] - You have more supported languages than language file paths")
            return False
        for _lang in config['lang']["files_paths"].keys():
            if not _lang in config['lang']['supported']:
                print(f"[CONFIG.JSON CRITICAL ERROR] - Add all your languages to the supported array. ({_lang} missing)")
                return False
            if not os.path.exists(config['lang']['files_paths'][_lang]):
                print(f"[CONFIG.JSON CRITICAL ERROR] - The path for the {_lang} language file does not exist.")
                return False
        if not config['lang']['ui'] in config['lang']['supported']:
            print("[CONFIG.JSON CRITICAL ERROR] - Your 'ui' language is not in the supported languages array.")
            return False
        w = windll.kernel32
        sl = windows_locale[w.GetUserDefaultUILanguage()]
        if sl[:2].lower() != config['lang']['system'].lower():
            print("[CONFIG.JSON CRITICAL ERROR] - Check if your system language is correct.")
            return False

        #Manager
        man_keys = config['manager'].keys()
        if len(man_keys) != 4:
            print("[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'manager'")
            return False
        for key in man_keys:
            if not key in ["priority", "title", "engine-path", "readme-path"]:
                print(f"[CONFIG.JSON CRITICAL ERROR] - '{key}' missing on manager settings.")
                return False
            if key == "priority":
                if not config['manager'][key] in ["idle", "below_normal", "normal", "above_normal", "high", "realtime"]:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The priority '{config['manager'][key]}' does not exist.")
                    return False
            if key == "engine-path":
                if not os.path.exists(config['manager'][key]):
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The engine path is not valid ({config['manager'][key]})")
                    return False

        #Server
        serv_keys = config['server'].keys()
        if len(serv_keys) != 7:
            print(f"[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'server'")
            return False
        for key in serv_keys:
            if not key in ["reboots", "name", "ip", "path", "cmd-title", "start-server-cmd-path", "priority"]:
                print(f"[CONFIG.JSON CRITICAL ERROR] - The key '{key}' is missing in 'server'")
                return False
            if key == "priority":
                if not config['server'][key] in ["idle", "below_normal", "normal", "above_normal", "high", "realtime"]:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The priority '{config['server'][key]}' does not exist.")
                    return False
            if key == "path" or key == "start-server-cmd-path":
                if not os.path.exists(config['server'][key]):
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The path {config['server'][key]} is not valid. ('server')")
                    return False
            if key == "reboots":
                reboots = config['server']['reboots']
                if type(reboots) == type([]) and len(reboots) > 0:
                    for _time in reboots:
                        try:
                            time.strptime(_time, '%H:%M')
                        except:
                            print(f"[CONFIG.JSON CRITICAL ERROR] - The reboot time {_time} is not a valid time. Use 24h format.")
                            return False
                else:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - Something is wrong with your 'reboots' array in 'server'. Check if you are using HH:MM in the 24h format.")
                    return False


        #Backup
        bck_keys = config['backup'].keys()
        if len(bck_keys) != 5:
            print(f"[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'backup'")
            return False
        for key in bck_keys:
            if not key in ["backup", "files", "location", "date", "prefix"]:
                print(f"[CONFIG.JSON CRITICAL ERROR] - The setting '{key}' is missing in 'backup'")
                return False
            if key == "backup":
                if not type(config['backup'][key]) == type(True):
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The key 'backup' on 'backup' must be 'true' or 'false'.")
                    return False
            if key == "date":
                if not type(config['backup'][key]) == type([]) and len(config['backup'][key]) == 3:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - Something is wrong with your 'date' array in 'backup'")
                    return False
            if key == 'files':
                __folders = []; __files = []
                for x in config['backup']['files']:
                    if x.find('.') > -1:
                        __files.append(x)
                    else:
                        __folders.append(x)
                for _FILE_ in __files:
                    _PATH_ = config['server']['path'] + '/' + _FILE_
                    if not os.path.isfile(_PATH_):
                        print(f"[CONFIG.JSON CRITICAL ERROR] - File '{_FILE_}' does not exist on path '{_PATH_}'")
                        return False
                for _FOLDER_ in __folders:
                    _PATH_ = config['server']['path'] + '/' + _FOLDER_
                    if not os.path.isdir(_PATH_):
                        print(f"[CONFIG.JSON CRITICAL ERROR] - Folder '{_FOLDER_}' does not exist on path '{_PATH_}'")
                        return False
            if key == "location":
                if not os.path.exists(config['backup'][key]):
                    print(f"[CONFIG.JSON CRITICAL ERROR] - Backups directory '{config['backup'][key]}' does not exist.")
                    return False
            if key == "prefix":
                word = config['backup']['prefix']
                for char in word:
                    if char in ["\\", "/", ":", "*", "?", "\"", "<", ">", "|"]:
                        print(f"[CONFIG.JSON CRITICAL ERROR] - You can't use the char '{char}' in the '[backup] prefix'")
                        return False

        #Log
        log_keys = config['log'].keys()
        if len(log_keys) != 5:
            print(f"[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'log'")
            return False
        for key in log_keys:
            if not key in ["log", "type", "location", "date", "prefix"]:
                print(f"[CONFIG.JSON CRITICAL ERROR] - The setting '{key}' is missing in 'log'")
                return False
            if key == "log":
                if not type(config['log'][key]) == type(True):
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The key 'log' in 'log' must be 'true' or 'false'.")
                    return False
            if key == "date":
                if not type(config['log'][key]) == type([]) and len(config['log'][key]) == 3:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - Something is wrong with the 'date' array in 'log'")
                    return False
            if key == "type":
                if not config['log'][key] in [".txt", ".md", ".docx", ".log", ".html", ".htm"]:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The file type '{config['log'][key]}' is not supported.")
                    return False
            if key == "location":
                if not os.path.isdir(config['log'][key]):
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The logs directory does not exist ({config['log'][key]})")
                    return False
            if key == "prefix":
                word = config['log']['prefix']
                for char in word:
                    if char in ["\\", "/", ":", "*", "?", "\"", "<", ">", "|"]:
                        print(f"[CONFIG.JSON CRITICAL ERROR] - You can't use '{char}' on the '[log] prefix'")
                        return False
        #Browser
        bw_keys = config['browser'].keys()
        if len(bw_keys) != 3:
            print(f"[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'browser'")
            return False
        for key in bw_keys:
            if not key in ["image", "close-on-start", "close-on-reboot"]:
                return False
            if key == "image":
                word = config['browser']['image']
                if word[-4:] != ".exe":
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The browser image is not an executable. (missing .exe)")
                    return False
            else:
                if type(config['browser'][key]) != type(True):
                    setting = "[browser] " + key
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The setting '{setting}' must be 'true' or 'false'")
                    return False

        #Test Server
        tserv_keys = config['test-server'].keys()
        if len(tserv_keys) != 7:
            print(f"[CONFIG.JSON CRITICAL ERROR] - A setting is missing in 'test-server'")
            return False
        if config['test-server']['active'] == None or config['test-server']['active'] != False:
            for key in tserv_keys:
                if not key in ["active", "name", "ip", "path", "cmd-title", "close-on-start", "close-on-reboot"]:
                    print(f"[CONFIG.JSON CRITICAL ERROR] - The setting '{key}' is missing in 'test-server'")
                    return False
                if key == "path":
                    if not os.path.isdir(config['test-server'][key]):
                        print(f"[CONFIG.JSON CRITICAL ERROR] - The 'test-server' path ({config['test-server'][key]}) does not exist.")
                        return False
                if key == "close-on-start" or key == "close-on-reboot":
                    if not type(config['test-server'][key]) == type(True):
                        print(f"[CONFIG.JSON CRITICAL ERROR] - The setting '{key}' on 'browser' must be 'true' or 'false'.")
                        return False

    except Exception as e:
        print(f"Your 'config.json' is broken. Try downloading it again.\n[ERROR] -> {e}")
        return False

    #Config is fine

    #Set lang var (already global)
    paths = config['lang']['files_paths']
    ui_lang = config['lang']['ui']
    with open(paths[ui_lang]) as file:
        lang = jload(file)[ui_lang]

    return True

def backup():
    if not config['backup']['backup']:
        return

    data = config['backup']['files']
    folders = []
    files = []

    for x in data:
        if x.find('.') > -1:
            files.append(x)
        else:
            folders.append(x)

    # Check if it's a file/folder
    for file in files:
        path = config['server']['path'] + '/' + file
        if os.path.isfile(path):
            continue
        else:
            log(lang["bck_res_err_file"].format(file, path))
            files.remove(file)

    for folder in folders:
        path = config['server']['path'] + '/' + folder
        if os.path.isdir(path):
            continue
        else:
            log(lang["bck_res_err_folder"].format(folder, path))
            folders.remove(folder)

    # Create backup dir and copy data
    location = config['backup']['location']
    if os.path.isdir(location):
        today = datetime.datetime.today()
        day = today.strftime("%d")
        month = today.strftime("%m")
        year = today.strftime("%y")

        hours = today.strftime('%H')
        minutes = today.strftime('%M')

        date_pattern = config['backup']["date"]
        pos = {
            "year":date_pattern.index("YYYY"),
            "day":date_pattern.index("DD"),
            "month":date_pattern.index("MM")
        }

        date = [day, month, year]
        time = hours + 'h' + minutes + 'm'
        for key in pos:
            date[pos[key]] = eval(key)

        name = config['backup']['prefix'] + ".".join(date) + f'({time})'
        abs = location + '/' + name

        if not os.path.exists(abs):
            os.mkdir(abs)
        else:
            log(lang["bck_loc_exists"].format(abs))
            return

        #Copy files/folders

        # engine.bat [backup] [src] [dst] [file|folder]
        # engine.bat [delete] [path] [file|folder]


        for file in files:
            src = '"' + config['server']['path'] + '/' + file + '"'
            dst = '"' + abs + '"'
            #subprocess.call(fr'{engine} backup {src} {dst} file')
            result = os.system(fr"copy {src} {dst}")
            if not result == 0:
                log(lang["bck_file_err"].format(file, dst))
            else:
                log(lang["bck_file_ok"].format(file, dst))

        for folder in folders:
            src = '"' + config['server']['path'] + '/' + folder + '"'
            dst = '"' + abs + '/' + folder + '"'
            #subprocess.call(fr'{engine} backup {src} {dst} folder')
            result = os.system(fr"xcopy {src} {dst} /S /C /Q /I")
            if not result == 0:
                log(lang["bck_folder_err"].format(folder, dst))
            else:
                log(lang["bck_folder_ok"].format(folder, dst))

    else:
        log(lang["bck_path_invalid"].format(location))
        return

def close_browser():
    image = config['browser']['image']

    pids = []

    for proc in psutil.process_iter():
        if proc.name() == image:
            pids.append(proc.pid)

    if len(pids) < 1:
        log(lang["b_already_cl"])
        return

    for pid in pids:
        os.kill(pid, signal.SIGTERM)

    log(lang["b_cl"])

def set_priority(pid=None, priority="normal"):
    import win32api,win32process,win32con

    priorities = {
        "idle":win32process.IDLE_PRIORITY_CLASS,
        "below_normal":win32process.BELOW_NORMAL_PRIORITY_CLASS,
        "normal":win32process.NORMAL_PRIORITY_CLASS,
        "above_normal":win32process.ABOVE_NORMAL_PRIORITY_CLASS,
        "high":win32process.HIGH_PRIORITY_CLASS,
        "realtime":win32process.REALTIME_PRIORITY_CLASS
    }
    if not pid:
        pid = os.getpid()
    handle = win32api.OpenProcess(win32con.PROCESS_ALL_ACCESS, True, pid)
    win32process.SetPriorityClass(handle, priorities[priority])
    log(lang["priority_set"].format(priority, pid))

def log(msg="", create=False):
    if not config['log']['log']:
        return

    _version = str(version)

    #Set time vars
    today = datetime.datetime.today()
    day = today.strftime("%d")
    month = today.strftime("%m")
    year = today.strftime("%y")

    hours = today.strftime('%H')
    minutes = today.strftime('%M')

    date_pattern = config['log']["date"]
    pos = {
        "year":date_pattern.index("YYYY"),
        "day":date_pattern.index("DD"),
        "month":date_pattern.index("MM")
    }

    date = [day, month, year]
    time = hours + 'h' + minutes + 'm'
    for key in pos:
        date[pos[key]] = eval(key)


    #Create log file
    if create:
        name = config['log']['prefix'] + ".".join(date) + f' ({time})'
        if os.path.isdir(config['log']['location']):
            abs = config['log']['location'] + '/' + name + config['log']['type']
            global file
            file = open(abs, "a+")

            #Initial print
            print("\n##########################################################")
            print("            " + lang["manager_started_caps"].format(str(_version)))
            print('##########################################################\n\n\n')
            print('=========================== ' +  lang["s_ini_caps"] + ' ===========================\n\n')
            print(f'[' + lang["man_strd_caps"] + f'] - {"/".join(date)} {time}')

            file.write("\n##########################################################")
            file.write("            " + lang["manager_started_caps"].format(str(_version)))
            file.write('##########################################################\n\n\n')
            file.write('GitHub - https://www.github.com/feRpicoral\n')
            file.write('FiveM Forum - https://www.example.com\n\n')
            file.write(lang["msg_1"])
            file.write(lang["msg_2"])
            file.write(lang["msg_3"])
            file.write(lang["msg_4"])
            file.write('=========================== ' +  lang["s_ini_caps"] + ' ===========================\n\n')
            file.write(f'[' + lang["man_strd_caps"] + '] {"/".join(date)} {time}')


        else:
            print(lang["log_folder_missing"].format(config['log']['location']))
            return

    #Log Message
    if file and msg:
        if msg == '\\n':
            print('\n'); file.write('\n')
            return

        #Log to CMD
        print(f'[{"/".join(date)} - {time}] {msg}\n')

        #Log to file
        file.write(f'[{"/".join(date)} - {time}] {msg}\n')

def get_window_pid(title):
    hwnd = win32gui.FindWindow(None, title)
    threadid,pid = win32process.GetWindowThreadProcessId(hwnd)
    return pid


class Server:
    def __init__(self):
        if config['log']['log']:
            log(create=True)
        self.pid = None #Set on start()
        self.rebt_dic = {} #Set on restart()
        self.looping = False #Allows main loop

    #TODO: Not being called anywhere
    def clear_cache(self):
        cache = '"' + config['server']['path'] + "/cache" + '"'
        #subprocess.call(fr'{engine} delete {cache} folder')
        os.system(fr"rmdir {cache} /S /Q")
        log('Main server\'s cache deleted successfully!')

    def start(self):
        # engine.bat [start] [cmd_title] [server_path] [start_cmd_path]
        main_running = True

        #Get strings and format
        title = config['server']['cmd-title']
        path = config['server']['path']
        path = r"" + path; path = path.replace("/", "\\")
        srt_path = config['server']["start-server-cmd-path"]

        #Prepare strings for start call
        arr = srt_path.split("/")
        srt_name = arr[-1]
        srt_arr = arr.remove(srt_name)
        srt_path = "\\".join(arr) + "\\"

        #Call Server
        os.system(fr'start "{srt_path}" {srt_name} "{title}" "{path}"')


        log(lang["s_started"])

        time.sleep(3) #Make sure the cmd window is open
        self.pid = get_window_pid(title)
        set_priority(self.pid, config['server']['priority'])

        #Close on start browser
        if config['browser']['close-on-start']:
            close_browser()

        #Close test server on start
        if config['test-server']['close-on-start']:
            if win32gui.FindWindowEx(None, None, None, config['test-server']['cmd-title']) != 0:
                test_pid = get_window_pid(config['test-server']['cmd-title'])
                os.kill(test_pid, signal.SIGTERM)
                log(lang["t_closed"])
                test_running = False
            else:
                log(lang["t_already_cl"])

        #Allows main loop
        self.looping = True
        self.main()

    #Return array of reboots in order of execution by time
    def sort_reboots(self):
        reboots = config['server']['reboots']
        self.rebt_dic = {}

        now = datetime.datetime.now().replace(microsecond=0)

        for time in reboots:
            hours = int(time[:2])
            minutes = int(time[-2:])
            #Not needed, validation just to be 110% sure
            if minutes > 59 or minutes < 00 or hours < 00 or hours > 23:
                reboots.remove(time)
                log(lang["invalid_time"].format(time))
                continue
            tdelta = datetime.datetime.strptime(time, "%H:%M") - now

            dif_h = str(tdelta.seconds//3600)
            dif_m = str(round((tdelta.seconds/3600 - int(dif_h)) * 60))

            if len(dif_h) == 1:
                dif_h = "0"+dif_h
            if len(dif_m) == 1:
                dif_m = "0"+dif_m

            dif = dif_h + ":" + dif_m

            self.rebt_dic[time] = {"dif":dif, "tdelta":tdelta}

        temp = []
        for time in self.rebt_dic:
            dif = self.rebt_dic[time]["dif"]
            temp.append(dif)
        temp = sorted(temp)

        reboots = [] #Restart var

        for dif in temp:
            for time in self.rebt_dic:
                if self.rebt_dic[time]["dif"] == dif:
                    reboots.append(time)
        del temp

        return reboots

    #Close the server
    def stop(self):
        os.kill(self.pid, signal.SIGTERM)
        log(lang["s_closed"])
        main_running = False

    #Calls stop() and start()
    def restart(self):

        self.stop()
        time.sleep(10)

        #Close on reboot browser
        if config['browser']['close-on-reboot']:
            close_browser()

        #Close test server on reboot
        if config['test-server']['close-on-start']:
            if win32gui.FindWindowEx(None, None, None, config['test-server']['cmd-title']) != 0:
                test_pid = get_window_pid(config['test-server']['cmd-title'])
                os.kill(test_pid, signal.SIGTERM)
                log(lang["t_closed"])
                test_running = False
            else:
                log(lang["t_already_cl"])

        time.sleep(3)

        self.start()

    #Main function. Handles the loop and calls the others functions
    def main(self):
        while self.looping:
            next_restart = self.sort_reboots()[0]
            time_gap_scnd = self.rebt_dic[next_restart]["tdelta"].seconds

            log(lang["nxt_reboot"].format(next_restart, time_gap_scnd))

            time.sleep(time_gap_scnd)

            self.restart()

            time.sleep(3)



if __name__ == '__main__':
    if os.path.exists(config_path):
        global main_running,test_running
        main_running = False; test_running = False

        if load_config(): #Returns true when no errors occur
            server = Server()
            server.start() #TODO: Create FiveM server and do some testing
        else:
            x = input("\nThe config.json could not be loaded.\nPress enter to exit...")
    else:
        print(f'\n###\n[CRITICAL ERROR] -> CONFIG FILE NOT FOUND ({config_path})!\n###\n')
        x = input("Press enter to exit...")
else:
    print("An unkown error happened. Please try again.\n")
    x = input("Press enter to exit...")
